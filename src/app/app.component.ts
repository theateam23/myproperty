import {Component, ViewChild} from '@angular/core';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {AlertController, App, Nav, Platform} from 'ionic-angular';

import {FirstRunPage} from '../pages/pages';
import {AngularFireAuth} from "angularfire2/auth";

@Component({
  templateUrl: 'template.html'
})
export class MyApp {
  rootPage = FirstRunPage;
  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'My Inbox', component: 'WelcomePage', icon:'ios-mail' },
    { title: 'People', component: 'CardPage', icon:'ios-people' },
    { title: 'Security', component: 'CardPage', icon:'ios-contact' },
    { title: 'My Bills', component: 'CardPage', icon:'ios-create' },
    { title: 'Trusted Vendor', component: 'CardPage', icon:'md-thumbs-up' },
    { title: 'Settings', component: 'CardPage', icon:'md-settings' }
  ];

  constructor(public fAuth: AngularFireAuth, platform: Platform, private statusBar: StatusBar, private splashScreen: SplashScreen, public  app: App, public alertCtrl: AlertController) {
    platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      platform.registerBackButtonAction(() => {
        let nav = app.getActiveNavs()[0];
        let activeView = nav.getActive();
        console.log("view ==> "+activeView.name);
        if(activeView.name === "ContentPage" || activeView.name==='WelcomePage') {
          if (nav.canGoBack()){
            nav.pop();
          } else {
            const alert = this.alertCtrl.create({
              title: 'Close this Application',
              message: 'Do you want to close the app?',
              buttons: [{
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                  console.log('Application exit prevented!');
                }
              },{
                text: 'Exit App',
                handler: () => {
                  platform.exitApp(); // Close this application
                }
              }]
            });
            alert.present();
          }
        }
      });

      /*this.fcm.getToken().then(token=>{
        console.log(token);
      });

      fcm.onNotification().subscribe( data => {
        if(data.wasTapped){
          //Notification was received on device tray and tapped by the user.
          console.log(JSON.stringify(data));
        }else{
          //Notification was received in foreground. Maybe the user needs to be notified.
          console.log(JSON.stringify(data));
        }
      });*/


    });
  }



  openPageWelcome() {
    this.nav.setRoot('WelcomePage');
  }

  logoutFirebase() {
    this.fAuth.auth.signOut();
  }
}
