import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ErrorHandler, NgModule} from "@angular/core";
import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig} from "@angular/platform-browser";
import {Camera} from "@ionic-native/camera";
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";
import {IonicStorageModule} from "@ionic/storage";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {IonicApp, IonicErrorHandler, IonicModule} from "ionic-angular";

import {MyApp} from "./app.component";
import {AngularFireModule} from "angularfire2";
import {AngularFireAuthModule} from "angularfire2/auth";
import {environment} from "../environment/environment";
import {Facebook} from "@ionic-native/facebook";
import {Api} from "../providers/api/api";
import {User} from "../providers/user/user";
import {IdmUserServices} from "../services/idmUserService";
import {TwitterConnect} from "@ionic-native/twitter-connect";
import {PropertyProviders} from "../providers/items/items";
import {UtilitiesProvider} from "../providers/utilities/utilities";
import {NativeStorage} from "@ionic-native/native-storage";
import {VisitorProvider} from "../providers/visitor/visitor";
import {Keyboard} from "@ionic-native/keyboard";
import {NgCalendarModule} from "ionic2-calendar";
import {FacilityProvider} from '../providers/facility/facility';
import {Firebase} from "@ionic-native/firebase";

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export class CustomHammerConfig extends HammerGestureConfig {
  overrides = {
    'press': { time: 2500 }  //set press delay for 1 second
  }
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp,{
      backButtonText: '',
      iconMode: 'ios',
      mode:'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      pageTransition: 'ios',
    }),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    NgCalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    Api,
    User,
    Camera,
    Keyboard,
    SplashScreen,
    StatusBar,
    // { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Facebook,
    IdmUserServices,
    TwitterConnect,
    PropertyProviders,
    UtilitiesProvider,
    { provide: HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig },
    NativeStorage,
    VisitorProvider,
    FacilityProvider,
    Firebase
  ]
})
export class AppModule { }
