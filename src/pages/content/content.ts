import {Component, NgZone} from "@angular/core";
import {Events, IonicPage, ModalController, NavController, Platform} from "ionic-angular";
import {PropertyProviders} from "../../providers/items/items";
import {IdmUserServices} from "../../services/idmUserService";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {NativeStorage} from "@ionic-native/native-storage";
import {DetailNewsPage} from "../pages";
import * as firebase from "firebase/app";
import {Firebase} from "@ionic-native/firebase";

@IonicPage()
@Component({
  selector: 'page-content',
  templateUrl: 'content.html'
})
export class ContentPage {
  cardItems=[];
  announcementItems= [{

  }];
  pageTitle:"";
  emailUser:String;
  offsetNews:number;
  propertyId:"";
  noNews:boolean;
  showDrop:boolean;
  profilePic:string;
  showing:boolean;
  showAnnouncement:boolean;

  constructor(public navCtrl: NavController,
              public property: PropertyProviders,
              private imUserService: IdmUserServices,
              public util : UtilitiesProvider,
              public modalCtrl: ModalController,
              private nativeStorage: NativeStorage,
              public events: Events,
              private zone: NgZone,
              public platform: Platform,
              public fBase:Firebase) {

    this.emailUser = firebase.auth().currentUser.email;
    this.property.getPropertyByEmail(this.emailUser).subscribe((data:any) => {
        console.log(data);
        if(data.isSuccess=='Y') {
          this.property._properties = data.property;
          if (data.property.length > 1) {
            this.showDrop = true;
          } else {
            this.showDrop = false;
          }
          if (this.platform.is('ios')) {
            this.fBase.grantPermission();
          }
          if(this.imUserService.getRole()==true){
            console.log("prepare subsscribe security~"+data.property[0].id);
            this.fBase.subscribe("security~"+data.property[0].id).then(dataSubscribe=>{
              console.log("sukses subscribe topic security")
            }).catch(error=>{
              console.error(error);
              console.log("gagal subscribe");
            });

          }
          //get from local storage if not found select first
          this.nativeStorage.getItem('propertySelected')
            .then(
              dataStorage => {
                console.log(dataStorage);
                this.pageTitle = dataStorage.name;
                this.propertyId = dataStorage.id;
                var inputNews = {
                  email: this.emailUser,
                  propertyId: dataStorage.id,
                  offset: this.offsetNews
                }
                this.loadNewsAndAnouncement(inputNews);
              },
              error => {
                console.error(error);
                this.nativeStorage.setItem("propertySelected",data.property[0]);
                this.pageTitle = data.property[0].name;
                this.propertyId = data.property[0].id;
                var inputNews = {
                  email: this.emailUser,
                  propertyId: this.propertyId,
                  offset: this.offsetNews
                }
                this.loadNewsAndAnouncement(inputNews);
              }
            );
        }
      },
      e => {
        console.log(e)
      }
    );
    this.events.subscribe('updateScreen', () => {
      this.zone.run(() => {
        console.log('force update the screen');
      });
    });

  }

  ionViewDidLoad() {
    console.log("load content");

  }

  ionViewDidEnter() {
    this.showing=false;
    let pic = '';
    if(this.imUserService.getPictureProfile()!=''&&this.imUserService.getPictureProfile()!=null){
      pic = this.imUserService.getPictureProfile();
    } else {
      pic = "assets/img/people.png";
    }

    if(pic.indexOf("http")<0 && pic.indexOf("assets")<0){
      pic = 'data:image/jpeg;base64,'+this.imUserService.getPictureProfile();
    }

    this.profilePic = pic;
  }

  loadNewsAndAnouncement(inputNews){
    this.property.getNewsByEmailAndPropId(inputNews).subscribe((data:any) => {
        // console.log("loadNewsAndAnouncement ===> "+JSON.stringify(data));
        if(data.isSuccess==="Y"){
          if(data.news.length>0) {
            // console.log(data.news);
            if(data.offset>0)
              this.cardItems.push(data.news);
            else
              this.cardItems = data.news;
            this.offsetNews = data.offset + 10;
            this.noNews = false;
          } else if(data.offset==0){
            this.cardItems = [];
            this.noNews = true;
          }

        }
      },
      e => {
        console.log(e)
      }
    );

    this.property.getAnnouncementByEmailAndPropId(inputNews).subscribe((data:any) => {
        // console.log("loadAnouncement ===> "+JSON.stringify(data));
        if(data.isSuccess==="Y"){
          if(data.announcement.length>0) {
            this.announcementItems = data.announcement;
            this.showAnnouncement = true;
            let elm = <HTMLElement>document.querySelectorAll(".scroll-content")[1];
            elm.style.marginTop = "30vh";
          } else {
            this.showAnnouncement = false;
          }
        }
      },
      e => {
        console.log(e)
      }
    );

  }

  detailNews(item){
    this.navCtrl.push(DetailNewsPage,{
      content: item
    });
  }

  loadMoreNews(){
    this.showing=true;
    var inputNews = {
      email: this.emailUser,
      propertyId: this.propertyId,
      offset:this.offsetNews
    }
    console.log("offestBaru ==> "+this.offsetNews);
    this.property.getNewsByEmailAndPropId(inputNews).subscribe((data:any) => {
        console.log("loadNewsAndAnouncement ===> "+JSON.stringify(data));
        this.showing=false;
        if(data.isSuccess=='Y'){
          if(data.news.length>0) {
            console.log(data.news);
            this.cardItems.push(data.news);
            this.offsetNews = data.offset+10;
          }
        }
      },
      e => {
        console.log(e)
      }
    );
  }

  presentModal(modalPage) {
    let modal = this.modalCtrl.create(modalPage, {propertyId: this.propertyId});
    modal.present();
    modal.onDidDismiss(data =>{
      if(data != undefined && data!=null){
        console.log("data after change title ===> "+ JSON.stringify(data));
        this.propertyId = data.id;
        var inputNews = {
          email: this.emailUser,
          propertyId: data.id,
          offset:0
        }
        this.nativeStorage.setItem("propertySelected",data);
        this.pageTitle = data.name;
        this.events.publish('updateScreen');
        console.log("prepare subsscribe news~"+data.id);
        this.fBase.subscribe("news~"+data.id).then(dataSubscribe=>{
          console.log("sukses subscribe topic news")
        }).catch(error=>{
          console.error(error);
          console.log("gagal subscribe news");
        });
        this.loadNewsAndAnouncement(inputNews);
      } else {
        console.log("cancel select property")
      }
    });
  }

}
