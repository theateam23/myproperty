import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Camera} from "@ionic-native/camera";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import * as firebase from "firebase/app";

/**
 * Generated class for the EditProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  formUser: { uid:string, name: string, lName:string, email: string,mobileNo:string,address:string,picture:string} = {
    name: '',
    lName: '',
    email: '',
    mobileNo:'',
    address:'',
    uid:'',
    picture:''
  };
  base64Image='assets/img/people.png';
  imageData='';
  constructor(public navCtrl: NavController, public viewCtrl:ViewController, public params: NavParams, public camera:Camera, public util:UtilitiesProvider) {
    this.formUser = this.params.get("user");
    this.base64Image = this.formUser.picture;
    this.formUser.email = firebase.auth().currentUser.email;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }

  dismiss(send) {
    if(send==='Y'){
      this.formUser.picture = this.imageData;
      this.viewCtrl.dismiss(this.formUser);
    } else {
      this.viewCtrl.dismiss(send);
    }
  }

  takePicture(){
    let options =
      {
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 512,
        targetHeight: 512,
        quality: 50,
        correctOrientation: true,
        allowEdit:true
      };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.imageData = imageData;
    }, (err) => {
      console.log(err);
    });
  }

}
