import {NgModule} from "@angular/core";
import {IonicPageModule} from "ionic-angular";
import {FacilityBookingPage} from "./facility-booking";
import {NgCalendarModule} from "ionic2-calendar";

@NgModule({
  declarations: [
    FacilityBookingPage,
  ],
  imports: [
    IonicPageModule.forChild(FacilityBookingPage),
    NgCalendarModule
  ],
})
export class FacilityBookingPageModule {}
