import {Component, ViewChild} from "@angular/core";
import {AlertController, IonicPage, ModalController, NavController, NavParams, Slides} from "ionic-angular";
import * as moment from "moment";
import {IdmUserServices} from "../../services/idmUserService";
import {PropertyProviders} from "../../providers/items/items";
import {NativeStorage} from "@ionic-native/native-storage";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {FacilityProvider} from "../../providers/facility/facility";

/**
 * Generated class for the FacilityBookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-facility-booking',
  templateUrl: 'facility-booking.html',
})
export class FacilityBookingPage {
  @ViewChild('facility') facilitySlide: Slides;
  facilities : any;
  eventSource = [];
  viewTitle: string;
  displayDate: string;
  displayDay: string;
  selectedDay = new Date();
  profilePic:string;
  facility:any;
  inputLoadBooking:any = {};
  listBookingperDay:any=[];
  calendar = {
    mode: 'month',
    currentDate: new Date(),
    showEvent:false
  };

  constructor(public navCtrl: NavController, public facilityServices:FacilityProvider,public util:UtilitiesProvider, public nativeStorage:NativeStorage,public propertyService:PropertyProviders,public imUser:IdmUserServices, private modalCtrl: ModalController, private alertCtrl: AlertController, public navParams: NavParams) {
    this.facilities = [];
    this.eventSource = []
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FacilityBookingPage');
  }

  ionViewDidEnter(){
    let pic = '';
    if(this.imUser.getPictureProfile()!=''&&this.imUser.getPictureProfile()!=null){
      pic = this.imUser.getPictureProfile();
    } else {
      pic = "assets/img/people.png";
    }

    if(pic.indexOf("http")<0 && pic.indexOf("assets")<0){
      pic = 'data:image/jpeg;base64,'+this.imUser.getPictureProfile();
    }

    this.profilePic = pic;

    this.nativeStorage.getItem('propertySelected')
      .then(
        dataStorage => {
          console.log(dataStorage);
          var inputProperty = {
            propertyId: dataStorage.id,
          };
          this.loadFacilities(inputProperty);
        },
        error => {
          console.error(error);

        }
      );

  }

  loadFacilities(input){
    this.propertyService.getFacilitiesByPropertyId(input).subscribe((resp:any)=>{
      if(resp.isSuccess=='Y'){
        this.facilities = resp.facilityList;
        //todo load booking list
        if(this.facilities.length>0) {
          this.facility = this.facilities[0];
          this.inputLoadBooking.facilityId = this.facility.id;
          this.getBookingList();
        } else {
          setTimeout(() => {
            this.eventSource = [];
          });
        }
      } else {
        this.util.loadToast(resp.message);
      }
    });
  }

  getBookingList(){
    this.facilityServices.getBookingListByDateRange(this.inputLoadBooking).subscribe((response:any)=>{
      let bookingList = [];
      if(response.isSuccess=='Y'){
          for(let i=0; i<response.bookingList.length; i++){
            let booking = response.bookingList[i];
            console.log(booking.code);
            let bookingMap :any={};
            bookingMap.title = booking.code + " " + booking.status;
            bookingMap.startTime = moment(booking.bookDate+" "+booking.startTime).toDate();
            bookingMap.endTime = moment(booking.bookDate+" "+booking.endTime).toDate();
            bookingList.push(bookingMap);
          }
        setTimeout(() => {
          this.eventSource = bookingList;
        });
      }

    });
  }

  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  onCurrentDateChanged(event:Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    // this.isToday = today.getTime() === event.getTime();
  }

  onEventSelected(event) {
    let start = moment(event.startTime).format('LLLL');
    let end = moment(event.endTime).format('LLLL');

    let alert = this.alertCtrl.create({
      title: '' + event.title,
      subTitle: 'From: ' + start + '<br>To: ' + end,
      buttons: ['OK']
    });
    alert.present();
  }

  onTimeSelected(ev) {
    console.log("onTimeSelected");
    this.displayDate = ev.selectedTime.getDate();
    this.displayDay = moment(ev.selectedTime).format('dddd');
    this.selectedDay = ev.selectedTime;

    this.listBookingperDay = ev.events;

    let currentMonth = moment(ev.selectedTime);
    this.inputLoadBooking = {
      startDate : currentMonth.startOf('month').format("YYYY-MM-DD"),
      endDate : currentMonth.endOf('month').format("YYYY-MM-DD")
    }

  }

  facilityChanged(){
    let currentIndex = this.facilitySlide.getActiveIndex();
    console.log(this.facilities[currentIndex]);
    this.facility = this.facilities[currentIndex];
    let currentMonth = moment(this.selectedDay);
    this.inputLoadBooking = {
      startDate : currentMonth.startOf('month').format("YYYY-MM-DD"),
      endDate : currentMonth.endOf('month').format("YYYY-MM-DD"),
      facilityId : this.facility.id
    };
    console.log(this.inputLoadBooking);
    //todo load booking list
    this.getBookingList();
  }

  getNewData(ev: { startTime: Date, endTime: Date }){
    if(this.facilities.length>0){
      let currentIndex = this.facilitySlide.getActiveIndex();
      console.log(this.facilities[currentIndex]);
      if(currentIndex==undefined){
        currentIndex = 0;
      }
      this.facility = this.facilities[currentIndex];
      let currentMonth = moment(ev.startTime);
      this.inputLoadBooking = {
        startDate : currentMonth.startOf('month').format("YYYY-MM-DD"),
        endDate : currentMonth.endOf('month').format("YYYY-MM-DD"),
      };
      this.inputLoadBooking.facilityId = this.facility.id;
      //todo load booking list
      console.log(this.inputLoadBooking);
      this.getBookingList();
    } else {
      setTimeout(() => {
        this.eventSource = [];
      });
    }
  }

  addBooking() {
    let modal = this.modalCtrl.create('AddBookFacilityPage', {listBooking:this.listBookingperDay, selectedDay: this.selectedDay,facility:this.facility});
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        data.bookDate = moment(this.selectedDay).format("YYYY-MM-DD");
        data.facilityId = this.facility.id;
        data.startTime = moment(data.startTime).format("HH:mm");
        data.endTime = moment(data.endTime).format("HH:mm");
        //to do save to backend
        this.util.presentLoading();
        this.facilityServices.addNewBooking(data).subscribe((resp:any)=>{
          if(resp.isSuccess=='Y'){
            let eventData = data;
            let startTime = data.bookDate +" " + moment(data.startTime).format("HH:mm");
            let endTime = data.bookDate +" " +moment(data.endTime).format("HH:mm");
            eventData.title=resp.code+" "+data.description;
            eventData.startTime = new Date(startTime);
            eventData.endTime = new Date(endTime);

            let events = this.eventSource;
            events.push(eventData);
            this.eventSource = [];
            setTimeout(() => {
              this.eventSource = events;
              this.util.dismissLoading();
            });
          } else {
            this.util.dismissLoading();
          }

        });

      }
    });
  }

}
