import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Camera} from "@ionic-native/camera";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IdmUserServices} from "../../services/idmUserService";
import * as moment from "moment";

/**
 * Generated class for the AddVisitorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-visitor',
  templateUrl: 'add-visitor.html',
})
export class AddVisitorPage {
  base64Image:string;
  visitorImage:string;
  visitorForm:FormGroup;
  isSecurity:boolean;
  constructor(public navCtrl: NavController,public imUser:IdmUserServices, public formBuilder: FormBuilder,public camera:Camera, public navParams: NavParams,public viewCtrl: ViewController) {
    this.base64Image = "assets/img/people.png";
    this.visitorImage = "";
    this.isSecurity = this.imUser.getRole();
    let now = moment('00:00','HH:mm');
    this.visitorForm = formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      timeVisit: [moment(now.format(), moment.ISO_8601).format(), Validators.compose([Validators.required])],
      dateVisit: ['', Validators.compose([Validators.required])],
      vehicleNo: ['', Validators.compose([])],
      phoneNo: ['', Validators.compose([Validators.required])],
    });

    if(this.isSecurity){
      this.visitorForm = formBuilder.group({
        name: ['', Validators.compose([Validators.required])],
        vehicleNo: ['', Validators.compose([])],
        phoneNo: ['', Validators.compose([Validators.required])],
      });
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddVisitorPage');
  }

  dismiss(send) {
      if(send==='Y'){
        if(this.visitorForm.valid){
          this.visitorForm.value.base64 = this.visitorImage;
          this.viewCtrl.dismiss(this.visitorForm.value);
        }
      } else {
        this.viewCtrl.dismiss(send);
      }
  }

  openGallery(){
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      quality:50,
      targetWidth: 512,
      targetHeight: 512,
      correctOrientation: true,
      allowEdit:true
    }).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.visitorImage = imageData;
      // console.log(imageData);
    }, (err) => {
      console.log(err);
    });
  }

  takePicture(){
    let options =
      {
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 512,
        targetHeight: 512,
        quality:50,
        correctOrientation: true,
        allowEdit:true
      };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is a base64 encoded string
      this.base64Image = "data:image/jpeg;base64," + imageData;
      this.visitorImage = imageData;
    }, (err) => {
      console.log(err);
    });
  }

}
