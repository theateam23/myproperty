import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {PropertyProviders} from "../../providers/items/items";

/**
 * Generated class for the AddNewPropertyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-new-property',
  templateUrl: 'add-new-property.html',
})
export class AddNewPropertyPage {
  properties:any=[];
  listPropertyOwn:any=[];
  selectedCheckBox=[];
  constructor(public navCtrl: NavController, public viewCtrl:ViewController, public property:PropertyProviders, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddNewPropertyPage');
  }

  ionViewDidEnter() {
    this.property.getAllProperty().subscribe((data:any) => {

        // console.log(data);
        this.property._properties.forEach((data:any)=>{
          this.listPropertyOwn.push(data.id);
        });
        if(data.isSuccess=='Y'){
          data.property.forEach((prop) => {
            if(!this.listPropertyOwn.includes(prop.id)){
                this.properties.push(prop)
            }
          });
          console.log(this.properties);
        }
      },
      e => {
        console.log(e)
      }
    );
  }

  getCheckBox(propId:any,isChecked){
    if(isChecked.checked) {
      this.selectedCheckBox.push(propId);
    } else {
      let idx = this.selectedCheckBox.indexOf(propId);
      this.selectedCheckBox.splice(idx,1);
    }
  }

  save(){
    console.log(this.selectedCheckBox);
    this.viewCtrl.dismiss(this.selectedCheckBox);
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
