import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {AddNewPropertyPage} from './add-new-property';

@NgModule({
  declarations: [
    AddNewPropertyPage,
  ],
  imports: [
    IonicPageModule.forChild(AddNewPropertyPage),
  ],
})
export class AddNewPropertyPageModule {}
