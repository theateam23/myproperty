// The page the user lands on after opening the app and without a session
export const FirstRunPage = 'WelcomePage';

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'TabsPage';

export const SignUpPage = 'SignupPage';

export const DetailNewsPage = 'DetailNewsPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'ContentPage';
export const Tab2Root = 'VisitorPage';
export const Tab3Root = 'FacilityBookingPage';
export const Tab4Root = 'ProfilePage';
