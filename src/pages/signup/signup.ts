import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AlertController, IonicPage, NavController, ToastController} from 'ionic-angular';
import {User} from '../../providers/providers';
import {FirstRunPage} from '../pages';
import {PropertyProviders} from "../../providers/items/items";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import * as firebase from "firebase/app";

interface PropertyItem {
  id: string
  name: any
}
type ListProperty = PropertyItem[];

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})

export class SignupPage {
  account: any = {};
  properties : ListProperty;
  selectedCheckBox = [];
  // account: { name: string, email: string, password: string };

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
              public user: User,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              private alertCtrl: AlertController,
              public property: PropertyProviders,
              public util:UtilitiesProvider) {


    this.property.getAllProperty().subscribe((data:any) => {
        this.util.dismissLoading();
        console.log(data);
        if(data.isSuccess=='Y'){
          this.properties = data.property;
        }
      },
      e => {
        console.log(e)
      }
    );


    var providerData = firebase.auth().currentUser.providerData[0];
    if(providerData!==undefined){


      this.account = providerData;
      this.account.name = providerData.displayName;
      this.account.mobileNo = providerData.phoneNumber;
    }
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }

  getCheckBox(propId:any,isChecked){
    if(isChecked.checked) {
      this.selectedCheckBox.push(propId);
    } else {
      var idx = this.selectedCheckBox.indexOf(propId);
      this.selectedCheckBox.splice(idx,1);
    }
  }

  doSignup() {
    this.account.propertyId = this.selectedCheckBox;
    this.util.presentLoading();
    this.user.signup(this.account).subscribe((resp:any) => {
      if(resp.isSuccess=="Y"){
        let alert = this.alertCtrl.create({
          title: 'Successfully Registration',
          subTitle: 'Please Wait Confirmation from your PM',
          buttons: [
            {
              text:'OK',
              handler: data => {
                this.navCtrl.push(FirstRunPage);
              }
            }
          ]
        });
        alert.present();
      } else {
        this.util.loadToast(resp.message);
      }
      this.util.dismissLoading();
    }, (err) => {
      let toast = this.toastCtrl.create({
        message: this.signupErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
}
