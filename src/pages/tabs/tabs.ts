import {Component} from '@angular/core';
import {IonicPage, MenuController, NavController} from 'ionic-angular';

import {Tab1Root, Tab2Root, Tab3Root, Tab4Root} from '../pages';
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import * as firebase from "firebase/app";
import {NativeStorage} from "@ionic-native/native-storage";
import {User} from "../../providers/user/user";

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;
  tab4Root: any = Tab4Root;

  tab1Title = "Dashboard";
  tab2Title = "Visitor";
  tab3Title = "Facility";
  tab4Title = "Profile";
  emailUser:String;
  constructor(public navCtrl: NavController, public user:User, public nativeStorage:NativeStorage, public util:UtilitiesProvider, public menuCtrl: MenuController) {
    this.menuCtrl.enable(true,'otherMenu');
  }

  callSOS(){
    navigator.vibrate(1000);
    //todo call SOS to backend
    this.nativeStorage.getItem('propertySelected')
      .then(
        dataStorage => {
          console.log(dataStorage);
          let input = {
                propertyId : dataStorage.id,
                email : firebase.auth().currentUser.email
          }
          this.user.callSOS(input).subscribe((data:any)=>{
            if(data.isSuccess=='Y'){
              this.util.loadAlert("Thank You", "Security Will reach you ASAP");
              navigator.vibrate(500);
            } else {
              this.util.loadAlert("Sorry", "Sorry  we cannot reach security right now, Please contact manually");
            }
          });

        },
        error => {
          console.error(error);

        }
      );

  }
}
