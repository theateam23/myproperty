import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import * as moment from "moment";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@IonicPage()
@Component({
  selector: 'page-add-book-facility',
  templateUrl: 'add-book-facility.html',
})
export class AddBookFacilityPage {

  booking: any  = { startTime: new Date(), endTime: new Date(), allDay: false };
  facility:any;
  selectedDate:any;
  minuteValues=[];
  hourValues=[];
  bookingForm:FormGroup;
  listBooking=[];

  constructor(public navCtrl: NavController,public formBuilder: FormBuilder, public navParams: NavParams, public viewCtrl: ViewController) {
    this.minuteValues = ['0']
    this.hourValues = ['8','9','10','11','12','13','14','15','16','17','18','19','20']
    this.facility = this.navParams.get('facility');
    this.booking.selectedDate = moment(this.navParams.get('selectedDay')).format('YYYY-MM-DD');
    this.booking.facilityId = this.facility?this.facility.id:'';
    let now = moment('00:00','HH:mm');
    this.bookingForm = formBuilder.group({
      startTime: [moment(now.format(), moment.ISO_8601).format(), Validators.compose([Validators.required])],
      endTime: [moment(now.format(), moment.ISO_8601).format(), Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])]

    });
    this.listBooking = this.navParams.get('listBooking');
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  save() {
    this.viewCtrl.dismiss(this.bookingForm.value);
  }

  /*getTimeInHours(){
    let times = [];
    let hour = 8; //start time to display
    let ap = ['am', 'pm'];
    let rangeTime = 12;

    for (var i=0;i<=rangeTime; i++) {
      times[i] = ("0" + ((hour==12)?12:hour%12)).slice(-2) + ':00 ' + ap[Math.floor(hour/12)];
      hour++;
    }
    return times;
  }*/

  ionViewDidEnter() {
    console.log('ionViewDidEnter AddBookFacilityPage');
    //to do get event
  }

}
