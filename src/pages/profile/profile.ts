import {Component, NgZone, ViewChild} from '@angular/core';
import {AlertController, App, Events, IonicPage, ModalController, Nav, NavController, NavParams} from 'ionic-angular';
import {User} from "../../providers/user/user";
import {IdmUserServices} from "../../services/idmUserService";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {Camera} from "@ionic-native/camera";
import * as firebase from "firebase/app";
import {NativeStorage} from "@ionic-native/native-storage";
import {Firebase} from "@ionic-native/firebase";
import {PropertyProviders} from "../../providers/items/items";

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  @ViewChild(Nav) nav: Nav;
  user: any;
  constructor(public navCtrl: NavController,public alertCtrl:AlertController,public property: PropertyProviders,public fBase:Firebase, public app:App, private nativeStorage: NativeStorage, public events:Events,private zone: NgZone, public modalCtrl:ModalController, public camera:Camera, public util:UtilitiesProvider, public navParams: NavParams, public userService:User, public imUser:IdmUserServices) {
    this.user = {};
    this.events.subscribe('updateScreenProfile', () => {
      this.zone.run(() => {
        console.log('force update the screen');
        let email = firebase.auth().currentUser.email;
        this.userService.getUser({email:email}).subscribe((resp:any) => {
            console.log("get user --->"+JSON.stringify(resp));
            if(resp.isSuccess =='Y'){
              this.user = resp.user;
              this.user.email = email;
              if(resp.user.imageUrl!=null || resp.user.imageUrl != undefined)
                this.base64Image = resp.user.imageUrl;
            }

          },e => {
            this.util.loadToast(JSON.stringify(e));
            console.log(e)
          }
        );
      });
    });

  }

  ionViewDidEnter() {
    let email = firebase.auth().currentUser.email;
    this.userService.getUser({email:email}).subscribe((resp:any) => {
        console.log("get user --->"+JSON.stringify(resp));
        if(resp.isSuccess =='Y'){
          this.user = resp.user;
          this.user.email = email;
          if(resp.user.imageUrl!=null || resp.user.imageUrl != undefined)
            this.base64Image = resp.user.imageUrl;
        }

      },e => {
        this.util.loadToast(JSON.stringify(e));
        console.log(e)
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  base64Image='assets/img/people.png';
  imageData='';

  presentModal(page){
    this.user.picture = this.base64Image;
    let modal = this.modalCtrl.create(page, {user: this.user});
    modal.present();
    modal.onDidDismiss(data => {
      if(data!="N"){
        this.user = data;
        if(data.picture==""){
          this.user.picture =  null;
        }
        this.userService.updateUser(this.user).subscribe((resp:any) => {
            if(resp.isSuccess =='Y'){
              this.imUser.setPictureProfile(this.user.picture);
              this.util.loadAlert("Success", "Your Profile successful updated");
              this.events.publish('updateScreenProfile');
            }

          },e => {
            this.util.loadToast(JSON.stringify(e));
            console.log(e)
          }
        );

      }
    });
  }

  addNewProperty(page){
    let modalProperty = this.modalCtrl.create(page, {user: this.user});
    modalProperty.present();
    modalProperty.onDidDismiss(data => {
      if(data){
        let inputData = {
          "email": firebase.auth().currentUser.email,
          "propertyIdList": data
        }
        this.property.updateProperty(inputData).subscribe((resp:any)=>{
          if(resp.isSuccess=="Y"){
            let alert = this.alertCtrl.create({
              title: resp.message,
              subTitle: 'Please Wait Confirmation from your PM',
              buttons: [
                {
                  text:'OK',
                  handler: data => {
                  }
                }
              ]
            });
            alert.present();
          } else {
            this.util.loadToast(resp.message);
          }
        })
      }
    });
  }

  doLogout() {

    let alert = this.alertCtrl.create({
      title: 'Sign Out',
      subTitle: 'Are You really want Sign Out?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('cancel log out');
        }
      },
        {
          text:'OK',
          handler: data => {
            if(this.imUser.getRole()){
              this.fBase.unsubscribe("security~"+this.property._properties[0].id);
            }
            firebase.auth().signOut();
            this.app.getRootNav().setRoot('WelcomePage');
            // getRootNav('WelcomePage');
            this.nativeStorage.remove('propertySelected');
            this.nativeStorage.remove('isLoggedIn');
          }
        }
      ]
    });
    alert.present();



  }
}
