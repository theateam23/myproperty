import {Component, NgZone} from "@angular/core";
import {Events, IonicPage, ModalController, NavController, NavParams, PopoverController} from "ionic-angular";
import {VisitorProvider} from "../../providers/visitor/visitor";
import {NativeStorage} from "@ionic-native/native-storage";
import {IdmUserServices} from "../../services/idmUserService";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import * as moment from "moment";
import * as firebase from "firebase/app";

/**
 * Generated class for the VisitorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-visitor',
  templateUrl: 'visitor.html',
})
export class VisitorPage {

  noVisitor:boolean;
  listVisitor:any[];
  propertyId:string;
  emailUser:string;
  isSecurity:boolean;

  profilePic:string;

  constructor(public navCtrl: NavController,
              public events: Events,
              private zone: NgZone,
              public util:UtilitiesProvider,
              public imUser:IdmUserServices,
              public nativeStorage:NativeStorage,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              public visitor:VisitorProvider,
              public popoverCtrl:PopoverController) {
    this.noVisitor = true;
    this.emailUser = firebase.auth().currentUser.email;
    this.listVisitor = [];

    this.events.subscribe('updateScreenVisitor', () => {
      this.zone.run(() => {
        console.log('force update the screen');
        this.nativeStorage.getItem('propertySelected')
          .then(data=>{
            var input = {
              email: this.emailUser,
              propertyId: data.id,
            };
            this.visitor.getListVisitor(input).subscribe((data:any) => {
                console.log(data);
                if(data.isSuccess=='Y'){
                  this.listVisitor = data.visitors;
                  if(data.visitors.length>0){
                    this.noVisitor=false;
                  } else {
                    this.noVisitor=true;
                  }
                }
              },
              e => {
                console.log(e)
              }
            );
          });
      });
    });
  }

  ionViewDidEnter() {
    let pic = '';
    if(this.imUser.getPictureProfile()!=''&&this.imUser.getPictureProfile()!=null){
      pic = this.imUser.getPictureProfile();
    } else {
      pic = "assets/img/people.png";
    }

    if(pic.indexOf("http")<0 && pic.indexOf("assets")<0){
      pic = 'data:image/jpeg;base64,'+this.imUser.getPictureProfile();
    }

    this.profilePic = pic;

    this.isSecurity = this.imUser.getRole();
    // this.isSecurity = true;
    console.log('ionViewDidEnter VisitorPage');
    this.nativeStorage.getItem('propertySelected')
      .then(data=>{
        this.propertyId = data.id;
        var input = {
          email: this.emailUser,
          propertyId: data.id,
        };
        this.visitor.getListVisitor(input).subscribe((data:any) => {
            console.log(data);
            if(data.isSuccess=='Y'){
              this.listVisitor = data.visitors;
              if(data.visitors.length>0){
                this.noVisitor=false;
              } else {
                this.noVisitor=true;
              }
            }
          },
          e => {
            console.log(e)
          }
        );
      });
  }
  search:{name:string}={name:''};
  searchVisitor(){

    console.log(this.search.name);
    this.util.presentLoading();
    var input = {
      email: this.emailUser,
      propertyId: this.propertyId,
      name:this.search.name
    };
    this.visitor.getListVisitor(input).subscribe((data:any) => {
        console.log(data);
        this.util.dismissLoading();
        if(data.isSuccess=='Y'){
          this.listVisitor = data.visitors;
          if(data.visitors.length>0){
            this.noVisitor=false;
          } else {
            this.noVisitor=true;
          }
        }
      },
      e => {
      this.util.dismissLoading();
        console.log(e)
      }
    );
  }

  presentModal(modalPage) {
    let modal = this.modalCtrl.create(modalPage);
    modal.present();
    modal.onDidDismiss(data=>{
      console.log("add visitor ==> "+ data);
      if(data!='N'){
        //todo save visitor, get data from form add visitor
        this.util.presentLoading();
        var input=data;
        input.propertyId = this.propertyId;
        let timeVisit = moment(data.timeVisit).format("HH:mm");
        input.date = data.dateVisit+" "+ timeVisit;
        if(data.dateVisit==null){
          input.date = moment(new Date()).format("YYYY-MM-DD HH:mm");
        }
        input.email = firebase.auth().currentUser.email;
        input.picture = data.base64;

        this.visitor.createVisitor(input).subscribe((data:any)=>{
          console.log(data);
          this.util.dismissLoading();
          if(data.isSuccess=='Y'){
            this.noVisitor=false;
            this.util.loadAlert("Success", "Successfully Insert new Visitor.");
            this.events.publish('updateScreenVisitor');
          }
        });
      }
    })
  }

  updateStatus(status:any, recordId:any){

    var input = {
      email:firebase.auth().currentUser.email,
      propertyId:this.propertyId,
      visitStatus:status,
      recordId:recordId
    };

    this.visitor.updateVisitor(input).subscribe((data:any) =>{
      console.log(data);
      if(data.isSuccess=='Y'){
        this.util.loadAlert("Success", "Successfully delete visitor.");
        this.events.publish('updateScreenVisitor');
      }
    });
  }



}
