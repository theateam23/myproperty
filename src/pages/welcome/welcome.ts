import {Component} from '@angular/core';
import {IonicPage, MenuController, ModalController, NavController, Platform, ToastController} from 'ionic-angular';
import {MainPage, SignUpPage} from "../pages";
import * as firebase from "firebase/app";
import {AngularFireAuth} from "angularfire2/auth";
import {Facebook} from "@ionic-native/facebook";
import {GooglePlus} from "@ionic-native/google-plus";
import {TwitterConnect} from "@ionic-native/twitter-connect";
import {IdmUserServices} from "../../services/idmUserService";
import {User} from "../../providers/providers";
import {UtilitiesProvider} from "../../providers/utilities/utilities";
import {PropertyProviders} from "../../providers/items/items";
import {NativeStorage} from "@ionic-native/native-storage";

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
  providers: [GooglePlus]
})
export class WelcomePage {

  account: any = {};
  fbColor: String ='#161D46';
  twColor: String ='#161D46';
  gColor: String ='#161D46';

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              public user: User,
              public toastCtrl: ToastController,
              private fAuth: AngularFireAuth,
              public nativeStorage: NativeStorage,
              private fb: Facebook,
              private googlePlus: GooglePlus,
              private imUserService: IdmUserServices,
              public twitterConnect: TwitterConnect,
              public util : UtilitiesProvider,
              public property: PropertyProviders,
              public platform: Platform,
              public menuCtrl: MenuController) {
    this.menuCtrl.enable(false,'otherMenu');
    firebase.auth().onAuthStateChanged(user=> {
      if (user) {
        // User is signed in.
        console.log("User is signed in");
        if (this.imUserService.authFlag == null || this.imUserService.authFlag.isEmpty() || !this.imUserService.authFlag.equals(user.uid)) {
          this.imUserService.authFlag=user.uid;
          this.util.presentLoading();
          this.checkUser(user);
        }
      } else {
        // No user is signed in.
        console.log("user not logged in, please login first");
        if (this.imUserService.authFlag != null) {
          this.imUserService.authFlag = null;
        }
      }
    })

  }

  // const unsubscribe =

  presentModal(modalPage) {
    let modal = this.modalCtrl.create(modalPage);
    modal.present();
    modal.onDidDismiss(data=>{
      console.log("reset password ==> "+ data);
      if(data.indexOf("@")>=0){
        return this.fAuth.auth.sendPasswordResetEmail(data);
      }
    })
  }

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  doLogin() {
    this.navCtrl.push(MainPage);

  }

  doSignup() {
    this.navCtrl.push('SignupPage');
  }

  async register(accountInfo:any) {
    try {
      const result = await this.fAuth.auth.createUserWithEmailAndPassword(
        accountInfo.email,
        accountInfo.password
      );
      if (result) {
        // this.util.presentLoading();
        let user = firebase.auth().currentUser;
        user.sendEmailVerification();
        this.imUserService.setIdmUser(result.uid,result.providerData[0]);
        // this.navCtrl.push(SignUpPage);
      }
    } catch (e) {
      // this.util.dismissLoading();
      this.util.loadToast(e.message);
      console.error(e.message);
    }
  }

  async loginEmail() {
    this.fAuth.auth.signOut();
    try {
      var r = await this.fAuth.auth.signInWithEmailAndPassword(
        this.account.email,
        this.account.password
      );
      if (r) {
        // this.util.presentLoading();
        // console.log("r===> "+JSON.stringify(r));
        if(r.emailVerified){
          this.imUserService.setIdmUser(r.uid,r.providerData[0]);
        } else {
          let user = firebase.auth().currentUser;
          user.sendEmailVerification();
          this.util.dismissLoading();
          this.util.loadAlert("Sorry...:(","Your Email Not verified, Please verify first");
        }
      }
    } catch (err) {
      // this.util.loadToast(err.message);
      console.error(err.code);
      if(err.code.indexOf("wrong-password") >= 0) {
        this.util.loadToast("Invalid email or password");
      } else if(err.code.indexOf("user-not-found") >= 0){
        this.register(this.account);
      }

    }
  }

  checkUser(response:any){
    this.account = firebase.auth().currentUser;

    this.user.getUser(this.account).subscribe((resp:any) => {
        // console.log("check user --->"+JSON.stringify(resp));
        this.util.dismissLoading();
        if(resp.isSuccess=="Y"){
          this.imUserService.setRole(resp.user.isSecurity);
          if(resp.user.imageUrl!=null || resp.user.imageUrl!= undefined)
            this.imUserService.setPictureProfile(resp.user.imageUrl);
          // this.nativeStorage.setItem("isLoggedIn",true);
          this.navCtrl.push(MainPage);
        } else {
          this.imUserService.setIdmUser(response.uid,response.providerData);
          this.navCtrl.push(SignUpPage);
        }

      },e => {
        this.util.dismissLoading();
        let toast = this.toastCtrl.create({
          message: "Server on maintenance, please retry again later.",
          duration: 3000,
          position: 'top'
        });
        toast.present();
        console.log(e)
      }
    );
  }

  async loginWithFacebook() {
    this.fAuth.auth.signOut();
    this.fbColor='#4EC2BE';
    this.gColor='#161D46';
    this.twColor = '#161D46';
    try{
      var result = await this.fb.login(['email']);
      // this.util.presentLoading();
      const fbCredential = firebase.auth.FacebookAuthProvider.credential(result.authResponse.accessToken);

      await firebase.auth().signInWithCredential(fbCredential).then(res => {
        this.imUserService.setIdmUser(res.uid,res.providerData[0]);
      });
    }catch(err){
      this.util.dismissLoading();
      console.error("error di FB " + err.errorMessage);
      this.util.loadToast(err.errorMessage)
    }

  }

  doLoginWithTwitter(){
    this.fAuth.auth.signOut();
    this.twColor='#4EC2BE';
    this.gColor = '#161D46';
    this.fbColor = '#161D46';
    this.twitterConnect.login().then((response:any) =>{
      console.log("sukses login twitter");
      // this.util.presentLoading();
      const twitterCredential = firebase.auth.TwitterAuthProvider.credential(response.token, response.secret);

      firebase.auth().signInWithCredential(twitterCredential)
        .then(res => {
          console.log(res);
          this.imUserService.setIdmUser(res.uid,res.providerData[0]);
        })
        .catch(error => {
          this.util.dismissLoading();
          this.util.loadToast(error.message);
        })
    }).catch(err => {
      this.util.dismissLoading();
      console.error(err);
      let toast = this.toastCtrl.create({
        message: JSON.stringify(err),
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }

  loginWithGoogle() {
    this.fAuth.auth.signOut();
    this.gColor='#4EC2BE';
    this.twColor = '#161D46';
    this.fbColor = '#161D46';
    let webClientId = '160573280185-id6e886k1spvkan20a0jv1sd796qpea6.apps.googleusercontent.com';
    if (this.platform.is('ios')) {
      webClientId = '160573280185-uip3decrufjp9d4kdmi8kf9kmu3t01f8.apps.googleusercontent.com';
    }
    this.googlePlus.login({
      'webClientId': webClientId,
      'offline': true
    }).then(res => {
      // this.util.presentLoading();
      const googleCredential = firebase.auth.GoogleAuthProvider.credential(res.idToken);

      firebase.auth().signInWithCredential(googleCredential).then( response => {
        this.imUserService.setIdmUser(response.uid,response.providerData[0]);
      }).catch(err => {
        console.error(JSON.stringify(err));
        let toast = this.toastCtrl.create({
          message: err,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      });


    }).catch(err => {
      this.util.dismissLoading();
        console.error(err);
        var errorMsg = "";
        if(err=="12510"){
          errorMsg = "User Cancelled";
        } else {
          errorMsg = "Unknown Error, Please Contact Admin";
        }
        this.util.loadToast(errorMsg);
      });
  }
}
