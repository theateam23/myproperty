import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {PropertyProviders} from "../../providers/items/items";

@IonicPage()
@Component({
  selector: 'page-list-property',
  templateUrl: 'list-property.html',
})
export class ListPropertyPage {

  properties = [];
  property : any;
  propertySelected : any;
  constructor(public navCtrl: NavController, public propertyProvider: PropertyProviders,public viewCtrl: ViewController, public params: NavParams) {
    this.properties = this.propertyProvider._properties;
    console.log("propertyId====> "+this.params.get("propertyId"));
    this.propertySelected = this.params.get("propertyId");
  }

  onChangeProperty(prop){
    this.property = prop;
  }

  dismiss(data){
    console.log(data);
    if(data==='Y'){
      this.viewCtrl.dismiss(this.property);
      this.propertySelected = this.property.id;
    } else {
      this.viewCtrl.dismiss();
    }
  }

}
