import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DetailNewsPage} from './detail-news';
import {DirectivesModule} from "../../directives/directives.module";

@NgModule({
  declarations: [
    DetailNewsPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailNewsPage),
    DirectivesModule
  ],
})
export class DetailNewsPageModule {}
