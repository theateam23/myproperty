import {Injectable} from '@angular/core';
import {Api} from "../api/api";


@Injectable()
export class FacilityProvider {
  _facilities = []
  constructor(public api:Api) {
    console.log('Hello FacilityProvider Provider');
  }

  getBookingListByDateRange(input:any){
    let seq = this.api.post('getBookingList',input).share();
    return seq;
  }

  addNewBooking(input:any){
    let seq = this.api.post('createBooking', input).share();
    return seq;
  }

}
