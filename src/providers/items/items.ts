import {Injectable} from '@angular/core';

import {Api} from '../api/api';

@Injectable()
export class PropertyProviders {
  _properties=[];
  _news=[];
  _announcement=[];
  constructor(public api: Api) { }

  getAllProperty(){
    let seq = this.api.post('getProperties',"").share();


    return seq;
  }

  getPropertyByEmail(email:any){
    let seq = this.api.post('getPropertiesByUser',{email:email}).share();
    seq.subscribe((res: any) => {
      if(res.isSuccess=='Y'){
        this._properties = res.property;
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  getNewsByEmailAndPropId(input:any){
    let seq = this.api.post('getNewsByUser',input).share();

    return seq;
  }

  getAnnouncementByEmailAndPropId(input:any){
    let seq = this.api.post('getAnnouncementByUser',input).share();

    return seq;
  }

  getFacilitiesByPropertyId(input:any){
    let seq = this.api.post('getFacilityByPropertyId',input).share();
    return seq;
  }

  updateProperty(input:any){
    return this.api.post('updateUserMobileProperties', input).share();
  }

}
